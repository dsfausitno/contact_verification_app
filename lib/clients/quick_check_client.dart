import 'package:http/http.dart' as http;
import 'package:http/http.dart';

class QuickCkeckClient {
  
  late String _baseUrl;
  late Duration _connectTimeout;

  QuickCkeckClient(){
    _baseUrl = 'https://0f64a98a-20a6-43a5-b325-5bc9aeafe5ca.mock.pstmn.io';
    _connectTimeout = const Duration(seconds: 20);
  }

  Future<Response> send(Object data) async {
    final url = Uri.parse('$_baseUrl/v1/quickcheck/send'); 
    return await http.post(
      url, 
      body: data
    ).timeout(_connectTimeout);
  }

  Future<Response> verify(Object data) async {
    final url = Uri.parse('$_baseUrl/v2/quickcheck/verify');
    return await http.post(
      url,
      body: data
    ).timeout(_connectTimeout);
  }
}