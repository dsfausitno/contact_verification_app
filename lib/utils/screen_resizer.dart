class ScreenResizer {
  static double getFormHorizontalPadding(double screenWidth){
    double horizontalPadding = 0;

    if (screenWidth < 400) {
      // Telas pequenas (smartphones)
      horizontalPadding = 16.0;
    } else if (screenWidth < 600) {
      // Telas médias (tablets)
      horizontalPadding = 32.0;
    } else {
      // Telas grandes (desktops)
      horizontalPadding = (screenWidth - 300) / 2;
    }

    return horizontalPadding;
  }

  static double getFormVerticalPadding(double screenHeight){
    double horizontalPadding = 0;

    if (screenHeight < 400) {
      // Telas pequenas (smartphones)
      horizontalPadding = 25.0;
    } else if (screenHeight < 700) {
      // Telas médias (tablets)
      horizontalPadding = 32.0;
    } else if (screenHeight < 1000) {
      // Telas grandes (desktops)
      horizontalPadding = (screenHeight - 650) / 3;
    } else {
      horizontalPadding = (screenHeight - 650) / 2;
    }

    return horizontalPadding;
  } 
}