class ErrorCodeParser {
  static String parse(String errorCode){
    switch (errorCode) {
      case 'CODE_INVALID':
        return 'O código está invalido.';
      case 'EMAIL_CLIENT_ERROR':
        return 'Não foi possível enviar a comunicação. Tente novamente mais tarde.';
      case 'UNEXPECTED_ERROR':
      default:
        return 'Tente novamente mais tarde.';
    }
  }
}