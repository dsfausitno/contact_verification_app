import 'package:flutter/material.dart';

class ColorTools {
  static Color getPrimaryColor(BuildContext context){
    final colorScheme = Theme.of(context).colorScheme;
    
    if(colorScheme.brightness == Brightness.dark){
      return colorScheme.inversePrimary;
    }

    return colorScheme.primary;
  }
}