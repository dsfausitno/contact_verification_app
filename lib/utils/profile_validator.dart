import 'package:contact_verification_app/models/profile_field.dart';
import 'package:contact_verification_app/models/profile_field_error.dart';
import 'package:contact_verification_app/models/profile.dart';

class ProfileValidator {
  static List<ProfileFieldError> validateProfile(Profile profile, String type){
    List<ProfileFieldError> result = [];

    if (type == 'email'){
      if(profile.email.isEmpty){
        result.add(_getEmptyError(ProfileField.email));
      } else {
        if(!_isValidEmail(profile.email)){
          result.add(_getInvalidError(ProfileField.email));
        }
      }
    }

    if (type == 'cellphone'){
      if(profile.cellphone.isEmpty){
        result.add(_getEmptyError(ProfileField.telefone));
      } else {
        if(!_isValidCellphone(profile.cellphone)){
          result.add(_getInvalidError(ProfileField.telefone));
        }
      }
    }

    if(profile.name.isEmpty){
      result.add(_getEmptyError(ProfileField.nome));
    }

    return result;
  }

  static ProfileFieldError _getEmptyError(ProfileField field){
    return ProfileFieldError(
      field: field, 
      message: 'O campo ${field.name} precisa ser preenchido.'
    );
  }

  static bool _isValidEmail(String email){
    String emailPattern = r'^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$';
    RegExp regex = RegExp(emailPattern);
    return regex.hasMatch(email);
  }

  static bool _isValidCellphone(String cellphone) {
    String cellphonePattern = r'^\(\d{2}\) \d{4,5}-\d{4}$';
    RegExp regex = RegExp(cellphonePattern);
    return regex.hasMatch(cellphone);
  }

  static ProfileFieldError _getInvalidError(ProfileField field){
    return ProfileFieldError(
      field: field, 
      message: 'O ${field.name} é inválido.'
    );
  }
}