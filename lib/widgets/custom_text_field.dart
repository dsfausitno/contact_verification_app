import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_multi_formatter/formatters/phone_input_formatter.dart';

class CustomTextField extends StatefulWidget {
  
  final TextEditingController controller;
  final TextInputType textInputType;
  final TextInputAction textInputAction;
  final bool isPassword;
  final String? hintText;
  final String? errorText;
  final IconData? icon;
  final Function(String)? onChanged;
  
  const CustomTextField({
    super.key,
    required this.controller,
    this.hintText,
    this.errorText,
    this.icon,
    this.onChanged,
    this.textInputType = TextInputType.text,
    this.textInputAction = TextInputAction.done,
    this.isPassword = false
  });

  @override
  State<CustomTextField> createState() => _CustomTextFieldState();
}

class _CustomTextFieldState extends State<CustomTextField> {

  bool showPassword = false;

  void toggleShowPassword(){
    setState(() => showPassword = !showPassword);
  }

  Widget getVisibilityIcon(){
    if(showPassword){
      return const Icon(Icons.visibility_off);
    }
    return const Icon(Icons.visibility);
  }

  List<TextInputFormatter>? getFormatters(){
    List<TextInputFormatter> formatters = <TextInputFormatter>[];
    if(widget.textInputType == TextInputType.phone){
      formatters.add(FilteringTextInputFormatter.digitsOnly);
      
      final phoneInputFormatter = PhoneInputFormatter(
        defaultCountryCode: 'BR'
      );
      formatters.add(phoneInputFormatter);
    }

    return formatters;
  } 

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        SizedBox(
          height: 90,
          child: TextField(
            onChanged: widget.onChanged,
            inputFormatters: getFormatters(),
            obscureText: widget.isPassword && !showPassword,
            keyboardType: widget.textInputType,
            controller: widget.controller,
            textInputAction: widget.textInputAction,
            decoration: InputDecoration(
              hintText: widget.hintText,
              errorText: widget.errorText == '' ? null : widget.errorText,
              border: const OutlineInputBorder(
                borderRadius: BorderRadius.all(Radius.circular(10))
              ),
              focusedBorder: OutlineInputBorder(
                borderRadius: const BorderRadius.all(Radius.circular(10)),
                borderSide: BorderSide(
                  width: 2.5,
                  color: Theme.of(context).colorScheme.primary
                )
              ),
              filled: true,
              fillColor: Theme.of(context).colorScheme.onSecondary,
              prefixIcon: widget.icon != null ? Icon(widget.icon): null,
              suffixIcon: widget.isPassword ? Transform.translate(
                offset: const Offset(-5, 0),
                child: IconButton(
                  onPressed: toggleShowPassword, 
                  icon: getVisibilityIcon()
                ),
              ): null,
            )
          ),
        )
      ],
    );
  }
}