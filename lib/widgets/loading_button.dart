import 'package:flutter/material.dart';

class LoadingButton extends StatefulWidget{
  
  final double width;
  final double height;
  final double afterRadius;
  final double beforeRadius;
  final double iconSize;
  final double labelSize; 
  final String label;
  final Function() onTap;
  final bool isLoading;
  final Color? backgroundColor;
  final Color? labelColor; 

  const LoadingButton({
    super.key,
    required this.width,
    required this.height,
    required this.afterRadius,
    required this.beforeRadius,
    required this.iconSize,
    required this.labelSize,
    required this.label,
    required this.onTap,
    this.isLoading = false,
    this.backgroundColor,
    this.labelColor
  });

  @override
  State<LoadingButton> createState() => _LoadingButtonState();
}

class _LoadingButtonState extends State<LoadingButton> {

  Widget getNextButtonContent(BuildContext context){
    if (widget.isLoading){
      return SizedBox(
        width: widget.iconSize,
        height: widget.iconSize,
        child: CircularProgressIndicator(
          strokeWidth: 3,
          color: widget.labelColor ?? Theme.of(context).colorScheme.onPrimaryContainer,
        )
      );
    }
    return Text(
      widget.label,
      style: TextStyle(
        fontSize: widget.labelSize,
        color: widget.labelColor ?? Theme.of(context).colorScheme.onPrimaryContainer,
      ), 
    );
  }

  @override
  Widget build(BuildContext context) {
    return AnimatedContainer(
      duration: const Duration(milliseconds: 300),
      width: widget.isLoading ? widget.height : widget.width,
      height: widget.height,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(widget.isLoading ? widget.afterRadius : widget.beforeRadius)
      ),
      child: ElevatedButton(
        onPressed: widget.isLoading ? () => {} : widget.onTap,
        style: ElevatedButton.styleFrom(
          backgroundColor: widget.backgroundColor ?? Theme.of(context).colorScheme.primary,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(widget.isLoading ? widget.afterRadius : widget.beforeRadius)
          ),
          padding: EdgeInsets.zero
        ),
        child: getNextButtonContent(context),
      ),
    );
  }
}