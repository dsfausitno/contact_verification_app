import 'package:flutter/material.dart';

class Toast extends StatelessWidget {
  final String message;
  final Color backgroundColor;
  final Color textColor;
  final Duration duration;

  const Toast({
    super.key,
    required this.message,
    this.backgroundColor = Colors.black,
    this.textColor = Colors.white,
    this.duration = const Duration(seconds: 3),
  });

  static void showToast(BuildContext context, String message, Color color) {
    final overlay = Overlay.of(context);
    const duration = Duration(seconds: 3);
    final overlayEntry = OverlayEntry(
      builder: (context) => Toast(
        message: message,
        duration: duration,
        backgroundColor: color,
      ),
    );

    overlay.insert(overlayEntry);

    Future.delayed(duration, () {
      overlayEntry.remove();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Material(
      color: Colors.transparent,
      child: Align(
        alignment: Alignment.bottomCenter,
        child: Container(
          margin: const EdgeInsets.all(20),
          padding: const EdgeInsets.symmetric(horizontal: 24, vertical: 12),
          decoration: BoxDecoration(
            color: backgroundColor,
            borderRadius: BorderRadius.circular(8),
            boxShadow: const [
              BoxShadow(
                color: Colors.black26,
                blurRadius: 10,
                offset: Offset(0, 4),
              ),
            ],
          ),
          child: Text(
            message,
            style: TextStyle(color: textColor),
          ),
        ),
      ),
    );
  }
}