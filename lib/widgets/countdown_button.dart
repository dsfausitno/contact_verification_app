import 'dart:async';

import 'package:flutter/material.dart';

class CountdownButton extends StatefulWidget {
  final DateTime targetTime;
  final Function() onTap; 
  final String label;

  const CountdownButton({
    super.key, 
    required this.targetTime,
    required this.onTap,
    required this.label
  });

  @override
  State<CountdownButton> createState() => _CountdownButtonState();
}

class _CountdownButtonState extends State<CountdownButton> {
  late Timer timer;
  int remainingSeconds = 0;

  @override
  void initState() {
    super.initState();
    startCountdown();
  }

  @override
  void didUpdateWidget(CountdownButton oldWidget) {
    super.didUpdateWidget(oldWidget);
    if (widget.targetTime != oldWidget.targetTime) {
      resetCountdown();
    }
  }

  void resetCountdown() {
    timer.cancel();
    startCountdown();
  }

  void startCountdown() {
    remainingSeconds = widget.targetTime.difference(DateTime.now()).inSeconds;

    timer = Timer.periodic(const Duration(seconds: 1), (timer) {
      setState(() {
        if (remainingSeconds > 0) {
          remainingSeconds--;
        } else {
          timer.cancel();
        }
      });
    });
  }

  @override
  void dispose() {
    timer.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return TextButton(
      onPressed: remainingSeconds == 0 ? widget.onTap : null,
      child: Text(
        remainingSeconds > 0
        ? "Reenviar em $remainingSeconds s"
        : widget.label,
        style: const TextStyle(
          fontSize: 16
        ),
      ),
    );
  }
}