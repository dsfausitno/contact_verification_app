import 'package:contact_verification_app/clients/quick_check_client.dart';
import 'package:contact_verification_app/models/problem_details.dart';
import 'package:contact_verification_app/models/quick_check_code_request.dart';
import 'package:contact_verification_app/models/quick_check_response.dart';
import 'package:contact_verification_app/models/quick_ckeck_verify_request.dart';

class QuickCheckService {
  late QuickCkeckClient _client;

  QuickCheckService(QuickCkeckClient client){
    _client = client;
  }

  Future<QuickCheckResponse> sendCode(QuickCheckCodeRequest request) async {
    try {
      final jsonBody = request.toJson();
      final response = await _client.send(jsonBody);
      
      if (response.statusCode.isSuccessful) {
        return QuickCheckResponse(success: true);
      }

      return _getResponse(response.body);
    } catch (e) {
      return _getUnexpectedError();
    }
  }

  Future<QuickCheckResponse> verifyCode(QuickCheckVerifyRequest request) async {
    try {
      final jsonBody = request.toJson();
      final response = await _client.verify(jsonBody);

      if (response.statusCode.isSuccessful) {
        return QuickCheckResponse(success: true);
      }

      return _getResponse(response.body);
    } catch (e) {
      return _getUnexpectedError();
    }
  }

  QuickCheckResponse _getResponse(String responseBody) {
    final problemDetails = ProblemDetails.fromJson(responseBody);

    return QuickCheckResponse(
      success: false,
      errorCode: problemDetails.code,
      errorMessage: problemDetails.detail
    );
  }

  QuickCheckResponse _getUnexpectedError(){
    return QuickCheckResponse(
      success: false,
      errorCode: 'UNEXPECTED_ERROR'
    );
  }
}
extension on int {
  bool get isSuccessful => this >= 200 && this <= 299;
}