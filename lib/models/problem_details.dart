import 'dart:convert';

class ProblemDetails {
  final int status;
  final String title;
  final String detail;
  final String instance;
  final String code;

  ProblemDetails({
    required this.status,
    required this.title,
    required this.detail,
    required this.instance,
    required this.code
  });

  factory ProblemDetails.fromJson(String body) {
    var json = jsonDecode(body);
    return ProblemDetails(
      status: json['status'],
      title: json['title'],
      detail: json['detail'],
      instance: json['instance'],
      code: json['code']
    );
  }
}