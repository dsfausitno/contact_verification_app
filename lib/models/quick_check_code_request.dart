import 'dart:convert';

class QuickCheckCodeRequest {

  String recipient;
  String type;
  
  QuickCheckCodeRequest({
    required this.recipient,
    required this.type
  });

  String toJson() => jsonEncode({
    'recipient': recipient, 
    'type': type
  });
}