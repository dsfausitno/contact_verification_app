class QuickCheckResponse {
  
  bool success;
  String? errorCode;
  String? errorMessage;

  QuickCheckResponse({
    required this.success,
    this.errorCode,
    this.errorMessage
  });
}