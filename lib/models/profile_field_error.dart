import 'package:contact_verification_app/models/profile_field.dart';

class ProfileFieldError {

  ProfileFieldError({
    required this.field,
    required this.message
  });

  ProfileField field;
  String message; 
}