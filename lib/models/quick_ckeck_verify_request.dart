import 'dart:convert';

class QuickCheckVerifyRequest {

  String recipient;
  String code;
  
  QuickCheckVerifyRequest({
    required this.recipient,
    required this.code
  });

  String toJson() => jsonEncode({
    'recipient': recipient,
    'code': code
  });
}