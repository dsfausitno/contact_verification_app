class Profile {

  Profile({
    required this.name,
    required this.email,
    required this.cellphone 
  });

  String name;
  String email;
  String cellphone;
}