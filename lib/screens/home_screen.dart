import 'package:contact_verification_app/utils/screen_resizer.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class HomeScreen extends StatefulWidget {

  final String name;

  const HomeScreen({
    super.key,
    required this.name
  });

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  Widget build(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width;
    double screenHeight = MediaQuery.of(context).size.height;
    String fisrtName = widget.name.split(' ')[0];

    return Scaffold(
      body: SingleChildScrollView(
        child: Center(
          child: Padding(
            padding: EdgeInsets.symmetric(
              vertical: ScreenResizer.getFormVerticalPadding(screenHeight),
              horizontal: ScreenResizer.getFormHorizontalPadding(screenWidth)
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                  padding: const EdgeInsets.only(bottom: 50),
                  alignment: Alignment.center,
                  child: Text(
                    '$fisrtName, obrigado por realizar o teste.',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontSize: 32,
                      color: Theme.of(context).colorScheme.onPrimaryContainer
                    ),
                  ),
                ),
                SvgPicture.asset(
                  'assets/celebration.svg',
                  height: 180.0,
                  width: 180.0,
                )
              ],
            ), 
          ),
        ),
      ),
    );
  }
}
