import 'package:contact_verification_app/clients/quick_check_client.dart';
import 'package:contact_verification_app/models/profile.dart';
import 'package:contact_verification_app/models/profile_field.dart';
import 'package:contact_verification_app/models/quick_check_code_request.dart';
import 'package:contact_verification_app/screens/verification_screen.dart';
import 'package:contact_verification_app/services/quick_check_service.dart';
import 'package:contact_verification_app/utils/color_tools.dart';
import 'package:contact_verification_app/utils/error_code_parser.dart';
import 'package:contact_verification_app/utils/screen_resizer.dart';
import 'package:contact_verification_app/utils/profile_validator.dart';
import 'package:contact_verification_app/widgets/custom_text_field.dart';
import 'package:contact_verification_app/widgets/loading_button.dart';
import 'package:contact_verification_app/widgets/toast.dart';
import 'package:flutter/material.dart';

class ProfileScreen extends StatefulWidget {
  
  const ProfileScreen({
    super.key
  });

  @override
  State<ProfileScreen> createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> {

  final service = QuickCheckService(QuickCkeckClient());

  final nameController = TextEditingController();
  final emailController = TextEditingController();
  final cellphoneController = TextEditingController();

  String nameError = '';
  String emailError = '';
  String cellphoneError = '';
  String selectedType = 'email';
  bool isLoading = false;

  @override
  void dispose(){
    super.dispose();

    nameController.dispose();
    emailController.dispose();
    cellphoneController.dispose();
  }
  
  void onNameChanged(String text){
    if (text.isNotEmpty){
      setState(() {
        nameError = '';
      });
    }
  }

  void onEmailChanged(String text){
    setState(() {
      emailError = '';
    });
  }

  void onCellphoneChanged(String text) {
    setState(() {
      cellphoneError = '';
    });
  }

  void onSend() async {
    Profile profile = Profile(
      name: nameController.text,
      cellphone: cellphoneController.text,
      email: emailController.text
    );

    var result = ProfileValidator.validateProfile(profile, selectedType);

    if(result.isEmpty){
      setState(() => isLoading = true);
      
      var recipient = selectedType == 'email' ? profile.email : profile.cellphone; 
      var request = QuickCheckCodeRequest(
        recipient: recipient, 
        type: selectedType
      );

      var response = await service.sendCode(request);

      setState(() => isLoading = false);

      if (response.success) {
        if(!mounted) return;
        Toast.showToast(
          context, 
          "Comunicação enviada com sucesso!", 
          Colors.green
        );

        await Future.delayed(const Duration(seconds: 3), () {
          if(!mounted) return;
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => 
              VerificationScreen(
                name: profile.name, 
                recipient: recipient,
                type: selectedType,
              )
            ),
          );
        });
      } else {
        var message = ErrorCodeParser.parse(response.errorCode!);
        if(!mounted) return;
        Toast.showToast(
          context, 
          message, 
          Colors.red
        );
      }
      return;
    }

    for(var fieldError in result){
      if(fieldError.field == ProfileField.email){
        setState(() => emailError = fieldError.message);
      } else if(fieldError.field == ProfileField.nome){
        setState(() => nameError = fieldError.message);
      } else if(fieldError.field == ProfileField.telefone) {
        setState(() => cellphoneError = fieldError.message);
      }
    }
  }

  @override
  Widget build(BuildContext context) {

    double screenWidth = MediaQuery.of(context).size.width;
    double screenHeight = MediaQuery.of(context).size.height;

    return Scaffold(
      body: SingleChildScrollView(
        child: Center(
          child: Padding(
            padding: EdgeInsets.symmetric(
              vertical: ScreenResizer.getFormVerticalPadding(screenHeight), 
              horizontal: ScreenResizer.getFormHorizontalPadding(screenWidth)
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Container(
                  padding: const EdgeInsets.only(bottom: 35),
                  alignment: Alignment.topCenter,
                  child: Text(
                    "Contato",
                    style: TextStyle(
                      fontSize: 32,
                      color: Theme.of(context).colorScheme.onPrimaryContainer
                    ),
                  ),
                ),
                CustomTextField(
                  controller: nameController,
                  hintText: "David Faustino",
                  icon: Icons.person,
                  textInputAction: TextInputAction.next,
                  onChanged: (text) => onNameChanged(text),
                  errorText: nameError,
                ),
                const Text(
                  'Qual tipo de contato será validado?',
                  style: TextStyle(
                    fontSize: 15
                  ),
                ),
                Wrap(
                  spacing: 15.0,
                  children: [
                    ChoiceChip(
                      label: const Text('E-mail'),
                      selected: selectedType == 'email',
                      onSelected: (bool selected) => setState(() => selectedType = 'email')
                    ),
                    ChoiceChip(
                      label: const Text('Telefone'),
                      selected: selectedType == 'cellphone',
                      onSelected: (bool selected) => setState(() => selectedType = 'cellphone'),
                    ),
                  ],
                ),
                const SizedBox(height: 20),
                Visibility(
                  visible: selectedType == 'email',
                  child: CustomTextField(
                    controller: emailController,
                    hintText: "exemple@exemple.com",
                    icon: Icons.email,
                    textInputType: TextInputType.emailAddress,
                    textInputAction: TextInputAction.next,
                    onChanged: (text) => onEmailChanged(text),
                    errorText: emailError,                  
                  )
                ),
                Visibility(
                  visible: selectedType == 'cellphone',
                  child: CustomTextField(
                    controller: cellphoneController,
                    hintText: "(00) 00000-0000",
                    icon: Icons.smartphone,
                    textInputType: TextInputType.phone,
                    textInputAction: TextInputAction.done,
                    onChanged: (text) => onCellphoneChanged(text),
                    errorText: cellphoneError,
                  )
                ),
                LoadingButton(
                  width: 130.0,
                  height: 40.0, 
                  afterRadius: 20.0, 
                  beforeRadius: 10.0, 
                  iconSize: 25.0, 
                  labelSize: 16.0, 
                  label: 'Enviar', 
                  onTap: () => onSend(),
                  isLoading: isLoading,
                  backgroundColor: ColorTools.getPrimaryColor(context),
                  labelColor: Colors.white,
                )
              ],
            ),
          )
        ),
      )
    );
  }
}
