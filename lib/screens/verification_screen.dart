import 'package:contact_verification_app/clients/quick_check_client.dart';
import 'package:contact_verification_app/models/quick_check_code_request.dart';
import 'package:contact_verification_app/models/quick_ckeck_verify_request.dart';
import 'package:contact_verification_app/screens/home_screen.dart';
import 'package:contact_verification_app/services/quick_check_service.dart';
import 'package:contact_verification_app/utils/color_tools.dart';
import 'package:contact_verification_app/utils/error_code_parser.dart';
import 'package:contact_verification_app/utils/screen_resizer.dart';
import 'package:contact_verification_app/widgets/countdown_button.dart';
import 'package:contact_verification_app/widgets/loading_button.dart';
import 'package:contact_verification_app/widgets/toast.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class VerificationScreen extends StatefulWidget {

  final String name;
  final String recipient;
  final String type;

  const VerificationScreen({
    super.key,
    required this.name,
    required this.recipient,
    required this.type
  });

  @override
  State<VerificationScreen> createState() => _VerificationScreenState();
}

class _VerificationScreenState extends State<VerificationScreen> {
  final service = QuickCheckService(QuickCkeckClient());
  
  final formKey = GlobalKey<FormState>();
  final List<FocusNode> focusNodes = List.generate(6, (_) => FocusNode());
  final List<TextEditingController> controllers = List.generate(6, (_) => TextEditingController());
  bool isLoading = false;
  DateTime freeDatetime = DateTime.now().add(const Duration(seconds: 60));

  void resend() async {
    var request = QuickCheckCodeRequest(
      recipient: widget.recipient, 
      type: widget.type
    );

    var response = await service.sendCode(request);

    late String message; 
    if (response.success) {
      message = 'Comunicação enviada com sucesso!';
      setState(() => freeDatetime = DateTime.now().add(const Duration(seconds: 60)));
    }
    else {
      message = ErrorCodeParser.parse(response.errorCode!);
    }

    if(!mounted) return;
    Toast.showToast(
      context, 
      message, 
      response.success ? Colors.green : Colors.red 
    );
  }

  void verifyCode() async {
    if (formKey.currentState!.validate()) {
      setState(() => isLoading = true);

      String code = controllers.map((controller) => controller.text).join();

      var request = QuickCheckVerifyRequest(
        recipient: widget.recipient, 
        code: code
      );
      var response = await service.verifyCode(request);

      setState(() => isLoading = false);

      late String message;
      if (response.success) {
        message = "Código verificado com sucesso!";
      } else {
        message = ErrorCodeParser.parse(response.errorCode!);
      }

      if(!mounted) return;
      Toast.showToast(
        context,
        message,
        response.success ? Colors.green : Colors.red 
      );

      if (response.success) {
        Future.delayed(const Duration(seconds: 3), () {
          if(!mounted) return;
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => HomeScreen(name: widget.name))
          );
        });
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width;
    double screenHeight = MediaQuery.of(context).size.height;

    return Scaffold(
      body: SingleChildScrollView(
        child: Center(
          child: Padding(
            padding: EdgeInsets.symmetric(
              vertical: ScreenResizer.getFormVerticalPadding(screenHeight),
              horizontal: ScreenResizer.getFormHorizontalPadding(screenWidth)
            ),
            child: Form(
              key: formKey,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                    padding: const EdgeInsets.only(bottom: 50),
                    alignment: Alignment.topCenter,
                    child: Text(
                      "Verificação",
                      style: TextStyle(
                        fontSize: 32,
                        color: Theme.of(context).colorScheme.onPrimaryContainer
                      ),
                    ),
                  ),
                  SvgPicture.asset(
                    'assets/message_sent.svg',
                    height: 150.0,
                    width: 150.0,
                  ),
                  const SizedBox(
                    height: 25,
                  ),
                  Text(
                    '${widget.name.split(' ')[0]}, insira o código de 6 dígitos enviado para você.',
                    style: const TextStyle(fontSize: 18),
                    textAlign: TextAlign.center,
                  ),
                  const SizedBox(height: 20),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: List.generate(6, (index) {
                      return SizedBox(
                        width: 45,
                        height: 80,
                        child: TextFormField(
                          controller: controllers[index],
                          focusNode: focusNodes[index],
                          maxLength: 1,
                          keyboardType: TextInputType.number,
                          textAlign: TextAlign.center,
                          decoration: const InputDecoration(
                            counterText: "",
                            border: OutlineInputBorder(),
                          ),
                          onChanged: (value) {
                            if (value.isNotEmpty && index < 5) {
                              FocusScope.of(context).requestFocus(focusNodes[index + 1]);
                            } else if (value.isEmpty && index > 0) {
                              FocusScope.of(context).requestFocus(focusNodes[index - 1]);
                            }
                          },
                          validator: (value) {
                            if (value == null || value.isEmpty) {
                              return "";
                            }
                            return null;
                          },
                        ),
                      );
                    }),
                  ),
                  const SizedBox(height: 25),
                  LoadingButton(
                    width: 120.0,
                    height: 40.0, 
                    afterRadius: 20.0, 
                    beforeRadius: 10.0, 
                    iconSize: 25.0, 
                    labelSize: 16.0, 
                    label: 'Verificar', 
                    onTap: verifyCode,
                    isLoading: isLoading,
                    backgroundColor: ColorTools.getPrimaryColor(context),
                    labelColor: Colors.white,
                  ),
                  const SizedBox(height: 5),
                  CountdownButton(
                    targetTime: freeDatetime, 
                    onTap: resend, 
                    label: 'Reenviar'
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
